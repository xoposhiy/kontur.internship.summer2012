using System;

namespace SummerInternshipTask.Checker
{
	/// <summary>
	/// ������������� �� ��������� [1 .. +inf) �����, ��� �������� ���� �������� �������� � [1 .. halfValue)
	/// 
	/// http://en.wikipedia.org/wiki/Pareto_distribution
	/// </summary>
	internal class ParetoDistribution
	{
		private readonly double k;

		public ParetoDistribution(double halfValue)
		{
			k = 1/Math.Log(halfValue, 2);
		}

		public double NextValue(Random r)
		{
			double p = r.NextDouble();
			return Math.Pow(1 - p, - 1/k);
		}
	}
}