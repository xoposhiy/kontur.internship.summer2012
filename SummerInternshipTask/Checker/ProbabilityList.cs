using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SummerInternshipTask.Checker
{
	/// <summary>
	/// List of items with probability associated with each item
	/// </summary>
	public class ProbabilityList<T> : IEnumerable<Tuple<T, double>>
	{
		private readonly List<Tuple<T, double>> items = new List<Tuple<T, double>>();

		public double Sum { get; private set; }

		public void AddEvent(T item, double probability)
		{
			items.Add(Tuple.Create(item, probability));
			Sum += probability;
		}
		
		public T SelectRandom(Random r)
		{
			double v = r.NextDouble() * Sum;
			double partialSum = 0.0;
			foreach (var item in items)
			{
				partialSum += item.Item2;
				if (partialSum > v) return item.Item1;
			}
			return items.Last().Item1;
		}

		public IEnumerable<Tuple<T, double>> ItemsInOrderFromNewToOld()
		{
			return Enumerable.Reverse(items);
		} 

		public IEnumerator<Tuple<T, double>> GetEnumerator()
		{
			return items.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}