﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SummerInternshipTask.Checker
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var randomSeed = ParseArg(args, 0, 123456); // this parameter will be changed!
			
			// other parameters will NOT be changed
			var recordsPerPage = ParseArg(args, 1, 50);
			var pagesInMemory = ParseArg(args, 2, 100);
			var categoriesCount = ParseArg(args, 3, 50);
			const int requestsCount = 5 * 1000 * 1000;
			var requests = GenerateRequests(randomSeed, requestsCount, categoriesCount);

			RunTests(recordsPerPage, pagesInMemory, requests, new MyCacheAlgorithm());
		}

		private static int ParseArg(string[] args, int argIndex, int defaultValue)
		{
			int value;
			if (args.Length > argIndex && int.TryParse(args[argIndex], out value))
				return value;
			return defaultValue;
		}

		public static void RunTests(int pageCapacity, int cacheCapacity, List<int> requests, params ICacheAlgorithm[] algorithms)
		{
			foreach (ICacheAlgorithm algorithm in algorithms)
			{
				var storage = new Storage(pageCapacity, cacheCapacity);
				algorithm.Init(storage);
				var sw = Stopwatch.StartNew();
				foreach (var req in requests)
				{
					algorithm.EnsurePageForThisKeyIsInCache(req);
					CheckPageIsActuallyInCache(storage, req);
				}
				long time = sw.ElapsedMilliseconds;

				var rating = requests.Count() / (storage.DiskReadsCount * 5.0 + time);
				Console.WriteLine(algorithm);
				Console.WriteLine("  rating {0}\t       ({1} ms, \t {2} disk reads)", rating, time, storage.DiskReadsCount);
			}
		}

		private static List<int> GenerateRequests(int randomSeed, int requestsCount, int categoriesCount)
		{
			Console.WriteLine("generating requests...");
			var gen = new Generator(categoriesCount, randomSeed);
			var requests = gen.Generate(requestsCount).ToList();
			Console.WriteLine("  Items   : {0}", gen.ItemsCount);
			Console.WriteLine("  Requests: {0}", requestsCount);
			Console.WriteLine();
			Console.WriteLine("processing requests...");
			return requests;
		}

		private static void CheckPageIsActuallyInCache(Storage storage, int key)
		{
			int? pageId = storage.FindAssignedPageId(key);
			if (pageId == null || !storage.PageIsInCache(pageId.Value))
				throw new Exception("page with the key '" + key + "' is not in memory!");
		}

	}
}
