using System;
using NUnit.Framework;

namespace SummerInternshipTask.Checker
{
	[TestFixture]
	public class ParetoDistributionTest
	{
		[Test]
		public void Test()
		{
			const int half = 10;
			const int count = 1000000;
			var r = new Random(12345);
			var law = new ParetoDistribution(half);
			int less = 0;
			int more = 0;
			for (int i = 0; i < count; i++)
			{
				double v = law.NextValue(r);
				Assert.GreaterOrEqual(v, 1);
				if (v <= half) less++;
				else more++;
			}
			Console.WriteLine(less + " " + more);
			Assert.IsTrue(Math.Abs(less - more) < 0.002*count);
		}
	}
}