using System;
using System.Collections.Generic;
using System.Linq;

namespace SummerInternshipTask.Checker
{
	public class Generator
	{
		private readonly int categoriesCount;
		private readonly ProbabilityList<int> categories = new ProbabilityList<int>();
		private readonly IDictionary<int, ProbabilityList<int>> items = new Dictionary<int, ProbabilityList<int>>();
		private readonly IList<int> chronoItems = new List<int>(); 
		private readonly ParetoDistribution itemsRating;
		private readonly ParetoDistribution catsRating;
		private int itemId;
		private readonly Random r;

		public Generator(int categoriesCount, int randomSeed)
		{
			this.categoriesCount = categoriesCount;
			r = new Random(randomSeed);
			itemsRating = new ParetoDistribution(200);
			catsRating = new ParetoDistribution(categoriesCount / 10.0);
			for(int iCategory=0; iCategory<categoriesCount; iCategory++)
			{
				categories.AddEvent(iCategory, catsRating.NextValue(r));
				items.Add(iCategory, new ProbabilityList<int>());
			}
			ItemsCount = 0;
		}

		public IEnumerable<int> Generate(int count)
		{
			int i = 0;
			while (i < count)
			{
				foreach (var req in NextRequestsBatch(count))
				{
					i++;
					yield return req;
				}
			}
		}

		public IEnumerable<int> GenerateFullScan()
		{
			List<int> all = items.Values.SelectMany(list => list).Select(itemProbability => itemProbability.Item1).ToList();
			for (int i = all.Count-1; i >=0; i--)
			{
				int randomPos = r.Next(i);
				var t = all[i];
				all[i] = all[randomPos];
				all[randomPos] = t;
			}
			return all;
		}

		public int ItemsCount { get; private set; }

		public IEnumerable<int> NextRequestsBatch(int count)
		{
			if (ItemsCount < 100 || Chance(0.3)) return UserCreatesSomeItem();
			if (Chance(0.0002)) return RobotScansArbitraryCategory();
			if (Chance(0.0005)) return RobotScansOldItems();
			if (Chance(0.6)) return UserOpensStartPage();
			return UserScansArbitraryCategory();
		}

		private IEnumerable<int> RobotScansArbitraryCategory()
		{
			var cat = r.Next(categoriesCount);
			return items[cat].Take(50).Select(itemProbability => itemProbability.Item1);
		}

		private IEnumerable<int> RobotScansOldItems()
		{
			return chronoItems.Take(200);
		}

		private IEnumerable<int> UserCreatesSomeItem()
		{
			var cat = categories.SelectRandom(r);
			var rate = itemsRating.NextValue(r);
			int key = cat + 1000 * (itemId++);
			items[cat].AddEvent(key, rate);
			ItemsCount++;
			chronoItems.Add(key);
			yield return key;
		}
		
		private IEnumerable<int> UserOpensStartPage()
		{ 
			//Request the latest item from each category
			return 
				categories
				.Select(category => items[category.Item1].ItemsInOrderFromNewToOld().FirstOrDefault())
				.Where(item => item != null).Select(itemProbability => itemProbability.Item1);
		}

		private IEnumerable<int> UserScansArbitraryCategory()
		{
			// Selects interesting category and reads some popular items from it
			var cat = categories.SelectRandom(r);
			int skippedCount = 0;
			foreach (var item in items[cat].ItemsInOrderFromNewToOld())
			{
				if (skippedCount < 20 && Chance(item.Item2 / items[cat].Sum))
				{
					yield return item.Item1;
					if (Chance(0.5)) yield break;
					skippedCount = 0;
				}
				else skippedCount++;
			}
		}

		public bool Chance(double probability)
		{
			return r.NextDouble() <= probability;
		}
	}
}