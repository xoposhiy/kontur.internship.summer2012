namespace SummerInternshipTask
{
	public interface ICacheAlgorithm
	{
		// ���������� � ����� ������, ��� �������������
		void Init(Storage storage);
		
		// ������ ��������� �������� � ������ ��� ������� ����� � ������� ������� storage
		void EnsurePageForThisKeyIsInCache(int key);
	}
}