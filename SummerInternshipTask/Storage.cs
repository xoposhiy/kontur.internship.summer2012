using System;
using System.Collections.Generic;

namespace SummerInternshipTask
{
	public class Storage
	{
		private readonly IDictionary<int, int> keyToPageId = new Dictionary<int, int>(); //key -> pageId
		private readonly IDictionary<int, int> pageSizes = new Dictionary<int, int>(); //pageId -> pageSize
		private readonly ISet<int> pagesInCache = new HashSet<int>();

		public Storage(int pageCapacity, int cacheCapacity)
		{
			DiskReadsCount = 0;
			PageCapacity = pageCapacity;
			CacheCapacity = cacheCapacity;
		}

		public int DiskReadsCount { get; private set; }
		public int PageCapacity { get; private set; }
		public int CacheCapacity { get; private set; }

		public void LoadOrCreatePage(int pageId)
		{
			if (pagesInCache.Contains(pageId)) return; // already loaded. nothing to do
			if (pagesInCache.Count >= CacheCapacity)
				throw new Exception("Cache overflow!");
			pagesInCache.Add(pageId);
			if (PageExists(pageId))
				DiskReadsCount++;
			else
				pageSizes.Add(pageId, 0);
		}

		public void UnloadPage(int pageId)
		{
			if (!pagesInCache.Remove(pageId))
				throw new Exception(string.Format("Page {0} is not in memory. Can't unload it", pageId));
		}

		public int PagesInCacheCount
		{
			get { return pagesInCache.Count; }
		}

		public bool PageIsInCache(int pageId)
		{
			return pagesInCache.Contains(pageId);
		}

		public void AssignKeyToPage(int pageId, int key)
		{
			if (keyToPageId.ContainsKey(key))
				throw new Exception("Key already is assigned to some page");
			if (!PageExists(pageId))
				throw new Exception("Unknown page. Call LoadOrCreatePage first");
			if (pageSizes[pageId] >= PageCapacity)
				throw new Exception("Overflow of the page " + pageId);
			pageSizes[pageId]++;
			keyToPageId.Add(key, pageId);
		}

		public int? FindAssignedPageId(int key)
		{
			int pageId;
			if (keyToPageId.TryGetValue(key, out pageId))
				return pageId;
			return null;
		}

		public int GetPageSize(int pageId)
		{
			int pageSize;
			if (pageSizes.TryGetValue(pageId, out pageSize)) return pageSize;
			return 0;
		}

		private bool PageExists(int pageId)
		{
			return pageSizes.ContainsKey(pageId);
		}
	}
}