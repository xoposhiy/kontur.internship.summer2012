using System;
using System.Collections.Generic;
using System.Linq;

namespace SummerInternshipTask
{
	// Fill free to modify this code as you wish :-)
	public class MyCacheAlgorithm : ICacheAlgorithm
	{
		private Storage storage;
		private int lastPageId;
		private readonly HashSet<int> loadedPages = new HashSet<int>();

		public void Init(Storage aStorage)
		{
			storage = aStorage;
		}

		public void EnsurePageForThisKeyIsInCache(int key)
		{
			var pageId = storage.FindAssignedPageId(key);
			if (pageId == null)
			{
				var pageForNewKey = SelectPageForKey(key);
				EnsurePageIsInCache(pageForNewKey);
				storage.AssignKeyToPage(pageForNewKey, key);
			}
			else
				EnsurePageIsInCache(pageId.Value);
		}

		private int SelectPageForKey(int key)
		{
			if (storage.GetPageSize(lastPageId) >= storage.PageCapacity)
				return ++lastPageId;
			return lastPageId;
		}

		public void EnsurePageIsInCache(int pageId)
		{
			if (storage.PageIsInCache(pageId)) return;
			if (storage.PagesInCacheCount >= storage.CacheCapacity)
				UnloadSomePage();
			PutPageInCache(pageId);
		}

		private void PutPageInCache(int pageId)
		{
			storage.LoadOrCreatePage(pageId);
			loadedPages.Add(pageId);
		}

		private void UnloadSomePage()
		{
			int pageToUnloadId = loadedPages.First();
			loadedPages.Remove(pageToUnloadId);
			storage.UnloadPage(pageToUnloadId);
		}
	}
}